package com.example.common;

public class Constant {
  final public static String COMMON_MONGO_CONFIG_BEAN = "commonMongoConfiguration";
  final public static String COMMON_MONGO_TEMPLATE = "commonMongoTemplate";
  final public static String MONGO_CLIENT_BEAN = "commonMongoClient";
  final public static String COUNTER_SEQUENCE_MONGO_TEMPLATE = COMMON_MONGO_TEMPLATE;
  final public static String COMMON_REDIS_CONFIG_BEAN = "commonRedisConfiguration";
  final public static String COMMON_REDIS_TEMPLATE = "commonRedisTemplateDefault";
  final public static String REDIS_SEQ_GEN_BEAN = "redisSequenceGenerator";
  final public static String MONGO_SEQ_GEN_BEAN = "mongoSequenceGenerator";
  final public static String KAFKA_PRODUCER_ROUTE_ENDPOINT = "direct:start:queue:";
  final public static String KAFKA_PRODUCER_FAILED_EXCHANGE = "failed-exchange";
  final public static String KAFKA_CONSUMER_INTERCEPTOR_BEAN = "consumerInterceptor";
  final public static String KAFKA_CONSUMER_INTERCEPTOR_METHOD = "consume";
  // mongo collection name
  final public static String COUNTER_SEQUENCE_COL_NAME = "counter_sequence";

  // order of beans
  // mongo beans
  final public static int MONGO_CLIENT_BEAN_ORDER = 0x7fffffff - 5000;
  final public static int COMMON_MONGO_TEMPLATE_ORDER = 0x7fffffff - 4990;
  // kafka consumer selector bean
  final public static int CONSUMER_FACTORY_ORDER = 0x7fffffff - 1000;
}
