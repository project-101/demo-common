package com.example.common.mongo;


import com.example.common.Constant;
import com.example.common.utils.sequence.CounterSequenceMongoRepository;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.annotation.Order;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.lang.NonNull;

@Data
@EqualsAndHashCode(callSuper = true)
@Configuration(Constant.COMMON_MONGO_CONFIG_BEAN)
@ConditionalOnProperty(value = "enable.mongo.common", havingValue = "true")
@ConfigurationProperties(prefix = "mongodb.common")
@EnableMongoRepositories(basePackageClasses = {CounterSequenceMongoRepository.class}, mongoTemplateRef = Constant.COMMON_MONGO_TEMPLATE)
public class CommonMongoConfiguration extends AbstractMongoClientConfiguration {

  private String connectionUri;
  private String database;

  @Lazy
  @NonNull
  @Override
  @Order(Constant.COMMON_MONGO_TEMPLATE_ORDER)
  @Bean(Constant.COMMON_MONGO_TEMPLATE)
  public MongoTemplate mongoTemplate() {
    return new MongoTemplate(this.mongoClient(), this.getDatabaseName());
  }

  @Lazy
  @NonNull
  @Override
  @Order(Constant.MONGO_CLIENT_BEAN_ORDER)
  @Bean(Constant.MONGO_CLIENT_BEAN)
  public MongoClient mongoClient() {
    return MongoClients.create(connectionUri);
  }

  @NonNull
  @Override
  protected String getDatabaseName() {
    return database;
  }
}