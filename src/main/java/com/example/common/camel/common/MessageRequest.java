package com.example.common.camel.common;

import lombok.Data;

import java.util.Map;

@Data
public class MessageRequest {

  @Data
  public static class Message {
    private Map<String, String> header;
    private Map<String, String> args;
    private String data;
  }

  private String topic;
  private String type;
  private String key;
  private String partition;
  private String offset;
  private Message message;

}