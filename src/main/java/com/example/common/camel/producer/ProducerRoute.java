package com.example.common.camel.producer;

import com.example.common.Constant;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@ConditionalOnProperty(value = "enable.producer.route", havingValue = "true")
public class ProducerRoute extends RouteBuilder {

  @Value("${camel.kafka.broker}")
  private String kafkaBroker;

  @Value("#{${camel.kafka.producer.global.properties}}")
  private Map<String, String> routeProp;

  @Value("#{${camel.kafka.producer.topic.properties}}")
  private List<Map<String, String>> topicProp;

  @Override
  public void configure() {
    if (!CollectionUtils.isEmpty(topicProp)) {
      topicProp.parallelStream().forEach(perTopic -> {
        // get topic and return if null as this is a required field
        String topic = perTopic.get("topic");
        if (!StringUtils.hasText(topic))
          return;

        // get global properties and merge topic wise properties
        Map<String, String> fromRoute = new HashMap<>(routeProp);
        fromRoute.putAll(perTopic);

        String produceFrom = Constant.KAFKA_PRODUCER_ROUTE_ENDPOINT + topic;
        StringBuilder produceTo = new StringBuilder("kafka:").append(topic).append("?brokers=").append(kafkaBroker);
        int threadCount = Integer.parseInt(perTopic.getOrDefault("threadCount", "1"));
        long delay = Integer.parseInt(perTopic.getOrDefault("delayTime", "0"));

        // remove properties that are applied programmatically
        fromRoute.remove("topic");
        fromRoute.remove("threadCount");
        fromRoute.remove("delayTime");

        // expanding properties
        if (!CollectionUtils.isEmpty(fromRoute)) {
          fromRoute.forEach((key, val) -> produceTo.append("&").append(key).append("=").append(val));
        }

        // creating route
        from(produceFrom).delay(delay).threads(threadCount).to(produceTo.toString());
      });
    }
  }
}
