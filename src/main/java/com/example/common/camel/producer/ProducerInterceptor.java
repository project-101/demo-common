package com.example.common.camel.producer;

import com.example.common.Constant;
import com.example.common.camel.common.MessageRequest;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.kafka.KafkaConstants;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.Map;

@Slf4j
@Service
@RequiredArgsConstructor
@ConditionalOnProperty(value = "enable.producer.route", havingValue = "true")
public class ProducerInterceptor {

  private final ProducerTemplate producerTemplate;
  private final ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT)
      .setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

  public String produce(String endpoint, String message, Object key) {
    Exchange exchangeMessage = producerTemplate.send(endpoint, ExchangePattern.InOut, exchange -> {
      exchange.setPattern(ExchangePattern.InOut);
      exchange.getIn().setBody(message);
      exchange.getIn().setHeader(KafkaConstants.KEY, key);
    });
    if (exchangeMessage.isFailed()) {
      log.error("Request for endpoint {} has been failed, message: {}", endpoint, message, exchangeMessage.getException());
      return Constant.KAFKA_PRODUCER_FAILED_EXCHANGE;
    }
    String exchangeId = exchangeMessage.getExchangeId();
    log.info("Request for endpoint: {} and message: {} has been placed with exchange id: {}", endpoint, message, exchangeId);
    return exchangeId;
  }

  public String produce(String endpoint, MessageRequest message, Object key) {
    String messageString;
    try {
      messageString = mapper.writeValueAsString(message);
    } catch (JsonProcessingException e) {
      log.error("cannot serialize the message: {}", message);
      return null;
    }
    return produce(endpoint, messageString, key);
  }

  public String produce(String topic, String type, Map<String, String> header, Map<String, String> args, Object data, String key) {
    String endpoint = Constant.KAFKA_PRODUCER_ROUTE_ENDPOINT + topic;
    MessageRequest messageRequest = new MessageRequest();
    messageRequest.setType(type);
    messageRequest.setTopic(topic);
    messageRequest.setKey(key);
    messageRequest.setMessage(new MessageRequest.Message());
    messageRequest.getMessage().setHeader(header);
    messageRequest.getMessage().setArgs(args);
    try {
      messageRequest.getMessage().setData(mapper.writeValueAsString(data));
    } catch (JsonProcessingException e) {
      log.error("cannot serialize the data: {}", data);
      return null;
    }
    return produce(endpoint, messageRequest, key);
  }

}
