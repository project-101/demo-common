package com.example.common.camel.consumer;

import com.example.common.Constant;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@ConditionalOnProperty(value = "enable.consumer.route", havingValue = "true")
public class ConsumerRoute extends RouteBuilder {

  @Value("${camel.kafka.broker}")
  private String kafkaBroker;

  @Value("${camel.kafka.consumer.group}")
  private String consumerGroup;

  @Value("#{${camel.kafka.consumer.global.properties}}")
  private Map<String, String> routeProp;

  @Value("#{${camel.kafka.consumer.topic.properties}}")
  private List<Map<String, String>> topicProp;

  @Override
  public void configure() {
    // consumer to properties
    StringBuilder consumerTo = new StringBuilder();
    consumerTo.append("bean:").append(Constant.KAFKA_CONSUMER_INTERCEPTOR_BEAN).append("?method=").append(Constant.KAFKA_CONSUMER_INTERCEPTOR_METHOD);
    // topic specific properties
    if (!CollectionUtils.isEmpty(topicProp)) {
      topicProp.parallelStream().forEach(perTopic -> {
        String topic = perTopic.get("topic");
        if (!StringUtils.hasText(topic))
          return;
        StringBuilder from = new StringBuilder();
        from.append("kafka:").append(topic)
            .append("?brokers=").append(kafkaBroker)
            .append("&groupId=").append(consumerGroup);
        // get global properties
        Map<String, String> fromRoute = new HashMap<>(routeProp);
        // append topic wise properties
        fromRoute.putAll(perTopic);
        fromRoute.remove("topic");
        fromRoute.forEach((key, val) -> from.append("&").append(key).append("=").append(val));
        from(from.toString()).errorHandler(noErrorHandler()).to(consumerTo.toString());
      });
    }

  }

}