package com.example.common.camel.consumer;

import com.example.common.Constant;
import com.example.common.camel.common.MessageRequest;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.component.kafka.KafkaConstants;
import org.apache.camel.component.kafka.KafkaManualCommit;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * This is the base class which is used to consume the topics that are defined in the route
 * ConsumerFactory will decide the bean which will process the message
 *
 * @see ConsumerRoute
 * @see ConsumerFactory
 * @see MessageProcessor
 */
@Slf4j
@Service(Constant.KAFKA_CONSUMER_INTERCEPTOR_BEAN)
@ConditionalOnProperty(value = "enable.consumer.route", havingValue = "true")
public class ConsumerInterceptor {

  private final ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT)
      .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

  public void consume(Exchange exchange) {
    MessageRequest messageRequest = null;
    try {
      messageRequest = mapper.readValue(exchange.getIn().getBody(String.class), MessageRequest.class);
      messageRequest.setPartition(exchange.getIn().getHeader(KafkaConstants.PARTITION, "0", String.class));
      messageRequest.setOffset(exchange.getIn().getHeader(KafkaConstants.OFFSET, "0", String.class));
      ConsumerFactory.instance(messageRequest.getTopic(), messageRequest.getType()).process(messageRequest);
    } catch (Exception e) {
      log.error("Error while processing exchange {}", exchange.getExchangeId(), e);
    } finally {
      commit(exchange, messageRequest);
    }
  }

  private void commit(Exchange exchange, MessageRequest messageRequest) {
    try {
      // commit manually if manual commit is enabled
      KafkaManualCommit committer = exchange.getIn().getHeader(KafkaConstants.MANUAL_COMMIT, KafkaManualCommit.class);
      if (committer != null)
        committer.commitSync();
    } catch (Exception ex) {
      log.error("Error committing the offset, message: {}", messageRequest, ex);
    }
  }

}
