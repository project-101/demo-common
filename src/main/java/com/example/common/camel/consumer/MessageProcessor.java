package com.example.common.camel.consumer;

import com.example.common.camel.common.MessageRequest;

public interface MessageProcessor {
  void process(MessageRequest message);
}
