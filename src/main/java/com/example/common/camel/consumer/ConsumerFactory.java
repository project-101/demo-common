package com.example.common.camel.consumer;

import com.example.common.Constant;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.annotation.Order;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@Lazy
@Configuration
@AllArgsConstructor
@Order(Constant.CONSUMER_FACTORY_ORDER)
@ConditionalOnProperty(value = "enable.consumer.route", havingValue = "true")
@ConfigurationProperties(value = "consumer.processor")
public class ConsumerFactory {

  private final ApplicationContext context;
  private final List<Style> selector;

  @Data
  public static class Style {
    public String topic;
    public String type;
    public String bean;
  }

  private final static Map<Pair<String, String>, MessageProcessor> processor = new HashMap<>();

  @PostConstruct
  public void init() {
    selector.parallelStream().forEach(each -> processor.put(
        Pair.of(StringUtils.trim(each.topic), StringUtils.trim(each.type)),
        context.getBean(each.bean, MessageProcessor.class)));
  }

  public static MessageProcessor instance(String topic, String type) {
    return processor.get(Pair.of(topic, type));
  }

}
