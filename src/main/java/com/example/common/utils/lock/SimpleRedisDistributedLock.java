package com.example.common.utils.lock;

import com.example.common.Constant;
import com.example.common.redis.CommonRedisConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.Objects;


@Lazy
@Service
@DependsOn(value = {Constant.COMMON_REDIS_CONFIG_BEAN})
@ConditionalOnProperty(value = "enable.redis.common", havingValue = "true")
public class SimpleRedisDistributedLock implements Lock {

  private final int lockingDatabase = 4;
  private final Duration defaultExpiry = Duration.ofMinutes(1);
  private static final String KEY_SUFFIX = ":lock";
  private final ReactiveRedisTemplate<String, Object> redisTemplate = CommonRedisConfiguration.reactiveRedisTemplate(lockingDatabase);

  @Override
  public boolean acquire(String key, String val) {
    return this.acquire(key, val, defaultExpiry);
  }

  @Override
  public boolean acquire(String key, String val, Duration expiry) {
    key = key + KEY_SUFFIX;
    return Boolean.TRUE.equals(redisTemplate.opsForValue().setIfAbsent(key, val, expiry).block());
  }

  @Override
  public boolean release(String key, String val) {
    key = key + KEY_SUFFIX;
    if (Objects.equals(redisTemplate.opsForValue().get(key).block(), val)) {
      return Boolean.TRUE.equals(redisTemplate.opsForValue().delete(key).block());
    }
    return false;
  }
}