package com.example.common.utils.lock;

import java.time.Duration;

public interface Lock {

  boolean acquire(String key, String val);

  boolean acquire(String key, String val, Duration expiry);

  boolean release(String key, String val);

}
