package com.example.common.utils;

import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service("commonHelper")
@AllArgsConstructor
public class Helper {
  public static ApplicationContext staticApplicationContext;
  private final ApplicationContext applicationContext;

  public static ApplicationContext getContext() {
    return staticApplicationContext;
  }

  @PostConstruct
  public void init() {
    staticApplicationContext = applicationContext;
  }

}
