package com.example.common.utils.sequence;

public interface SequenceGenerator {

  long generateId(Class<?> tClass);

  long generateId(String key);

  void setId(Class<?> tClass, long value);

  void setId(String key, long value);

}
