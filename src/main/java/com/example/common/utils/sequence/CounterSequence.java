package com.example.common.utils.sequence;

import com.example.common.Constant;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Data
@AllArgsConstructor
@Document(Constant.COUNTER_SEQUENCE_COL_NAME)
public class CounterSequence implements Serializable {

  private static final long serialVersionUID = 3433210890518207370L;
  @Id
  private String id;
  private long value;
}
