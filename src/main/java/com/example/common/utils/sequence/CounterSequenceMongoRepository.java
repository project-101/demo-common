package com.example.common.utils.sequence;

import com.example.common.Constant;
import com.example.common.utils.Helper;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
@DependsOn(Constant.COUNTER_SEQUENCE_MONGO_TEMPLATE)
@ConditionalOnProperty(value = "enable.mongo.common", havingValue = "true")
public interface CounterSequenceMongoRepository extends MongoRepository<CounterSequence, String> {
  MongoTemplate mongoTemplate = Helper.getContext().getBean(Constant.COUNTER_SEQUENCE_MONGO_TEMPLATE, MongoTemplate.class);
  FindAndModifyOptions modifyOptions = new FindAndModifyOptions().returnNew(true).upsert(true);
  String _id = "_id";
  String value = "value";

  default long increaseSequence(String id) {
    Query query = new Query();
    query.addCriteria(Criteria.where(_id).is(id));
    Update update = new Update();
    update.inc(value);
    CounterSequence sequence = mongoTemplate.findAndModify(query, update, modifyOptions, CounterSequence.class, Constant.COUNTER_SEQUENCE_COL_NAME);
    assert sequence != null;
    return sequence.getValue();
  }

}
