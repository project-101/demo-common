package com.example.common.utils.sequence;

import org.springframework.data.redis.support.atomic.RedisAtomicLong;

public class RedisSequence {
  public RedisAtomicLong sequence;
  public RedisAtomicLong lastSync;
  public String lockValue;
}