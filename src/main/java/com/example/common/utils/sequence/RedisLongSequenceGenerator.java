package com.example.common.utils.sequence;

import com.example.common.Constant;
import com.example.common.utils.lock.Lock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.support.atomic.RedisAtomicLong;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import static com.example.common.redis.CommonRedisConfiguration.REDIS_CONNECTION_SEQUENCE_GENERATOR_BEAN_NAME;

@Primary
@Service(Constant.REDIS_SEQ_GEN_BEAN)
@ConditionalOnProperty(value = {"enable.mongo.common", "enable.redis.common"}, havingValue = "true")
public class RedisLongSequenceGenerator implements SequenceGenerator {

  private static final ConcurrentHashMap<String, RedisSequence> sequenceMapper = new ConcurrentHashMap<>();
  private static final String LAST_SYNC = ":last_sync";
  private final static String KEY_PREFIX = "RedisSequenceGenerator:";
  private static final int SYNC_DIFF = 1000;
  private static final int TOLERANCE = (int) (SYNC_DIFF * 1.05); // should be greater than SYNC_DIFF
  private static final int INITIAL_VALUE = 1; // should be greater than equal to 1
  private static final int OFFSET = 0; // change to vary starting point of sequence
  private final CounterSequenceMongoRepository sequenceRepository;
  private final RedisConnectionFactory redisConnectionFactory;
  private final Lock lock;
  private RedisLongSequenceGenerator sequenceGenerator;

  public RedisLongSequenceGenerator(CounterSequenceMongoRepository sequenceRepository,
                                    @Qualifier(REDIS_CONNECTION_SEQUENCE_GENERATOR_BEAN_NAME) RedisConnectionFactory redisConnectionFactory, Lock lock) {
    this.sequenceRepository = sequenceRepository;
    this.redisConnectionFactory = redisConnectionFactory;
    this.lock = lock;
    buildSequence();
  }

  private void buildSequence() {
    for (CounterSequence sequence : sequenceRepository.findAll()) {
      RedisSequence redisSequence = new RedisSequence();
      redisSequence.sequence = createOrGetRedisAtomicLong(sequence.getId(), sequence.getValue() + TOLERANCE);
      redisSequence.lastSync = createOrGetRedisAtomicLong(sequence.getId() + LAST_SYNC, sequence.getValue());
      sequenceMapper.put(sequence.getId(), redisSequence);
    }
  }

  @Lazy
  @Autowired
  public void setSequenceGenerator(RedisLongSequenceGenerator sequenceGenerator) {
    this.sequenceGenerator = sequenceGenerator;
  }

  @Override
  public long generateId(Class<?> _class) {
    return this.generateId(KEY_PREFIX + _class.toString());
  }

  @Override
  public long generateId(String key) {
    long sequence = sequenceMapper.computeIfAbsent(key, sequenceGenerator::fetchFromMongo).sequence.getAndIncrement();
    // when redis key got flushed or redis restart
    if (sequence == 0) {
      sequenceMapper.put(key, sequenceGenerator.fetchFromMongo(key));
      sequence = sequenceMapper.get(key).sequence.getAndIncrement();
    }
    // flush to mongo db
    flushToMongo(sequenceMapper.get(key));
    return sequence + OFFSET;
  }

  @Override
  public void setId(Class<?> _class, long value) {
    this.setId(KEY_PREFIX + _class.toString(), value);
  }

  @Override
  public void setId(String key, long value) {
    RedisSequence redisSequence = sequenceMapper.computeIfAbsent(key, k -> new RedisSequence());
    redisSequence.sequence = new RedisAtomicLong(key, redisConnectionFactory, value);
    redisSequence.lastSync = new RedisAtomicLong(key + LAST_SYNC, redisConnectionFactory, Long.MIN_VALUE);
    flushToMongo(redisSequence);
  }

  public void flushToMongo(RedisSequence redisSequence) {
    if ((redisSequence.sequence.get() - redisSequence.lastSync.get()) > SYNC_DIFF) {
      String random = UUID.randomUUID().toString();
      if (lock.acquire(redisSequence.sequence.getKey(), random, Duration.ofSeconds(3))) {
        redisSequence.lockValue = random;
        sequenceGenerator.flushToMongo(redisSequence.sequence.getKey());
      }
    }
  }

  @Async
  public void flushToMongo(String id) {
    final RedisSequence redisSequence = sequenceMapper.get(id);
    try {
      Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
      sequenceRepository.findById(id).ifPresent(sequence -> {
        if (redisSequence.sequence.get() > sequence.getValue()) {
          sequence.setValue(redisSequence.sequence.get());
          sequenceRepository.save(sequence);
          redisSequence.lastSync.set(sequence.getValue());
        }
      });
    } finally {
      lock.release(redisSequence.sequence.getKey(), redisSequence.lockValue);
    }
  }

  public RedisSequence fetchFromMongo(String id) {
    RedisSequence redisSequence = new RedisSequence();
    Optional<CounterSequence> counterSequence = sequenceRepository.findById(id);
    if (counterSequence.isPresent()) {
      redisSequence.sequence = createOrGetRedisAtomicLong(id, counterSequence.get().getValue() + TOLERANCE);
      redisSequence.lastSync = createOrGetRedisAtomicLong(id + LAST_SYNC, counterSequence.get().getValue());
    } else {
      redisSequence.sequence = createOrGetRedisAtomicLong(id, INITIAL_VALUE);
      redisSequence.lastSync = createOrGetRedisAtomicLong(id + LAST_SYNC, INITIAL_VALUE);
    }
    // lock on saving object
    String random = UUID.randomUUID().toString();
    if (lock.acquire(id, random, Duration.ofSeconds(3))) {
      sequenceRepository.save(new CounterSequence(id, redisSequence.sequence.get()));
      lock.release(id, random);
    }
    return redisSequence;
  }

  private RedisAtomicLong createOrGetRedisAtomicLong(String key, long value) {
    RedisAtomicLong redisAtomicLong = new RedisAtomicLong(key, redisConnectionFactory);
    if (redisAtomicLong.get() == 0) {
      // lock on setting new value others will have key and they can fetch new value safely
      String random = UUID.randomUUID().toString();
      if (lock.acquire(key, random, Duration.ofSeconds(1))) {
        redisAtomicLong.set(value);
        lock.release(key, random);
      }
    }
    return redisAtomicLong;
  }

}