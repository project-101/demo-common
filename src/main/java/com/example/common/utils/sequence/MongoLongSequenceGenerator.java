package com.example.common.utils.sequence;

import com.example.common.Constant;
import com.example.common.utils.lock.Lock;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.UUID;

@Order
@AllArgsConstructor
@Service(Constant.MONGO_SEQ_GEN_BEAN)
@ConditionalOnProperty(value = {"enable.mongo.common", "enable.redis.common"}, havingValue = "true")
public class MongoLongSequenceGenerator implements SequenceGenerator {

  private final static String KEY_PREFIX = "MongoSequenceGenerator:";
  private final CounterSequenceMongoRepository sequenceRepository;
  private final Lock lock;

  @Override
  public long generateId(Class<?> _class) {
    return this.generateId(KEY_PREFIX + _class.toString());
  }

  @Override
  public long generateId(String key) {
    return sequenceRepository.increaseSequence(key);
  }

  @Override
  public void setId(Class<?> _class, long value) {
    this.setId(KEY_PREFIX + _class.toString(), value);
  }

  @Override
  public void setId(String key, long value) {
    key = KEY_PREFIX + key;
    String random = UUID.randomUUID().toString();
    if (lock.acquire(KEY_PREFIX + key, random, Duration.ofSeconds(1))) {
      try {
        sequenceRepository.save(new CounterSequence(key, value));
      } finally {
        lock.release(KEY_PREFIX + key, random);
      }
    }
  }

}
