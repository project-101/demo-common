package com.example.common.redis;

import com.example.common.Constant;
import com.example.common.utils.Helper;
import io.lettuce.core.resource.ClientResources;
import lombok.Data;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisSentinelConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceClientConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.util.StringUtils;

import java.time.Duration;

import static io.lettuce.core.ReadFrom.NEAREST;
import static org.springframework.beans.factory.config.BeanDefinition.SCOPE_PROTOTYPE;

@Data
@Configuration(Constant.COMMON_REDIS_CONFIG_BEAN)
@ConditionalOnProperty(value = "enable.redis.common", havingValue = "true")
@ConfigurationProperties(prefix = "redis.common.sentinel")
@EnableRedisRepositories(redisTemplateRef = Constant.COMMON_REDIS_TEMPLATE)
public class CommonRedisConfiguration {

  public final static String REDIS_CONNECTION_BEAN_NAME = "commonRedisConnectionFactory";
  public final static String REACTIVE_REDIS_CONNECTION_BEAN_NAME = "commonReactiveRedisConnectionFactory";
  public final static String REDIS_CONFIGURATION_BEAN_NAME = "commonRedisSentinelConfiguration";
  public final static String REDIS_TEMPLATE_BEAN_NAME = "commonRedisTemplateDefault";
  public final static String LETTUCE_CONFIGURATION_BEAN_NAME = "commonLettuceClientConfiguration";
  public final static String REDIS_CONNECTION_SEQUENCE_GENERATOR_BEAN_NAME = "commonRedisSequenceConnectionFactory";

  private String master;
  private String nodes;
  private String password;
  private int database;
  private int sequenceDatabase = 2;

  public static <K, V> RedisTemplate<K, V> redisTemplate(int dbIndex) {
    RedisTemplate<K, V> template = new RedisTemplate<>();
    template.setConnectionFactory(lettuceConnectionFactory(dbIndex));
    template.setKeySerializer(new StringRedisSerializer());
    template.setHashKeySerializer(new Jackson2JsonRedisSerializer<>(Object.class));
    template.setValueSerializer(new Jackson2JsonRedisSerializer<>(Object.class));
    template.setHashValueSerializer(new Jackson2JsonRedisSerializer<>(Object.class));
    template.afterPropertiesSet();
    return template;
  }

  public static ReactiveRedisTemplate<String, Object> reactiveRedisTemplate(int dbIndex) {
    RedisSerializationContext.RedisSerializationContextBuilder<String, Object> builder =
        RedisSerializationContext.newSerializationContext(new StringRedisSerializer());
    return new ReactiveRedisTemplate<>(lettuceConnectionFactory(dbIndex), builder.value(new GenericToStringSerializer<>(Object.class)).hashValue(new Jackson2JsonRedisSerializer<>(Object.class)).build());
  }

  @Scope(SCOPE_PROTOTYPE)
  @Bean(REDIS_CONNECTION_BEAN_NAME)
  public RedisConnectionFactory lettuceConnectionFactory(@Qualifier(REDIS_CONFIGURATION_BEAN_NAME) RedisSentinelConfiguration redisSentinelConfiguration,
                                                         @Qualifier(LETTUCE_CONFIGURATION_BEAN_NAME) LettuceClientConfiguration lettuceClientConfiguration) {
    return new LettuceConnectionFactory(redisSentinelConfiguration, lettuceClientConfiguration);
  }

  @Scope(SCOPE_PROTOTYPE)
  @Bean(REACTIVE_REDIS_CONNECTION_BEAN_NAME)
  public ReactiveRedisConnectionFactory lettuceReactiveConnectionFactory(@Qualifier(REDIS_CONFIGURATION_BEAN_NAME) RedisSentinelConfiguration redisSentinelConfiguration,
                                                                         @Qualifier(LETTUCE_CONFIGURATION_BEAN_NAME) LettuceClientConfiguration lettuceClientConfiguration) {
    return new LettuceConnectionFactory(redisSentinelConfiguration, lettuceClientConfiguration);
  }

  public static LettuceConnectionFactory lettuceConnectionFactory(int dbIndex) {
    LettuceConnectionFactory connectionFactory = (LettuceConnectionFactory) Helper.getContext().getBean(REDIS_CONNECTION_BEAN_NAME, RedisConnectionFactory.class);
    connectionFactory.setDatabase(dbIndex);
    connectionFactory.afterPropertiesSet();
    return connectionFactory;
  }

  @Scope(SCOPE_PROTOTYPE)
  @Bean(REDIS_CONFIGURATION_BEAN_NAME)
  public RedisSentinelConfiguration redisSentinelConfiguration() {
    RedisSentinelConfiguration redisSentinelConfiguration = new RedisSentinelConfiguration(master, StringUtils.commaDelimitedListToSet(nodes));
    redisSentinelConfiguration.setPassword(RedisPassword.of(password));
    redisSentinelConfiguration.setDatabase(database);
    return redisSentinelConfiguration;
  }

  @Scope(SCOPE_PROTOTYPE)
  @Bean(REDIS_TEMPLATE_BEAN_NAME)
  public <K, V> RedisTemplate<K, V> redisTemplate() {
    return redisTemplate(database);
  }

  @Bean(LETTUCE_CONFIGURATION_BEAN_NAME)
  public LettuceClientConfiguration lettuceClientConfiguration() {
    return LettuceClientConfiguration.builder()
        .readFrom(NEAREST)
        .clientResources(ClientResources.create())
        .commandTimeout(Duration.ofSeconds(3))
        .build();
  }

  @Bean(REDIS_CONNECTION_SEQUENCE_GENERATOR_BEAN_NAME)
  public RedisConnectionFactory redisSequenceConnectionFactory() {
    return lettuceConnectionFactory(sequenceDatabase);
  }

}
