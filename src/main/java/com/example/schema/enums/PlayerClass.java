package com.example.schema.enums;

public enum PlayerClass {
  WARRIOR, MAGICIAN, HEALER, TANK;
}
