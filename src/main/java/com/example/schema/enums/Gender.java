package com.example.schema.enums;

public enum Gender {
  MALE, FEMALE;
}
