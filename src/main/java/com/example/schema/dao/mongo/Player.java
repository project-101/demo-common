package com.example.schema.dao.mongo;

import com.example.schema.dto.PlayerMongoDTO;
import com.example.schema.enums.Gender;
import com.example.schema.enums.PlayerClass;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.util.Date;

@Data
@Document("players")
public class Player {

  private long id;

  @Field("first_name")
  private String firstName;

  @Field("last_name")
  private String lastName;

  @Field("gender")
  private Gender gender;

  @Field("birthday")
  private Date birthday;

  @Field("player_class")
  private PlayerClass playerClass;

  @Field("intelligence")
  private int intelligence;

  @Field("strength")
  private int strength;

  public PlayerMongoDTO playerDTO() {
    PlayerMongoDTO player = new PlayerMongoDTO();
    BeanUtils.copyProperties(this, player);
    return player;
  }

}
