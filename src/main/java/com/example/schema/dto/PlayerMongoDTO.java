package com.example.schema.dto;

import com.example.schema.dao.mongo.Player;
import com.example.schema.enums.Gender;
import com.example.schema.enums.PlayerClass;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.util.Date;

@Data
public class PlayerMongoDTO {

  private long id;

  private String firstName;

  private String lastName;

  private Gender gender;

  private Date birthday;

  private PlayerClass playerClass;

  private int intelligence;

  private int strength;

  public Player playerMongoDao() {
    Player player = new Player();
    BeanUtils.copyProperties(this, player);
    return player;
  }

}
