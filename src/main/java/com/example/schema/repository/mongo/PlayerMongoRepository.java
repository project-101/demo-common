package com.example.schema.repository.mongo;

import com.example.schema.dao.mongo.Player;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PlayerMongoRepository extends MongoRepository<Player, Long> {
}
